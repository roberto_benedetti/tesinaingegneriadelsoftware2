# DEVELOPMENT OF A SOFTWARE FOR MANAGING SOCCER TOURNAMENTS

Project work for the exam of Software Engineering, the application was made in Java featuring various design patterns along with a relational database for storing data.
Full work doc can be found below.

//

Progetto di tesina per l'esame di Ingegneria del Software, l'applicazione � stata realizzata in Java sfruttando diversi design pattern insieme ad un database per memorizzare i dati.
Documentazione completa in fondo alla pagina.

[Documentation (italian)](https://bitbucket.org/roberto_benedetti/tesinaingegneriadelsoftware2/raw/c7f24372ebac0e2c4960b50e081ceeb7d81e399e/Tesina_Ingegneria_del_Software.pdf)
