package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Torneo;

class TestNumeroPartite {

	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		// Al momento della sua creazione in un torneo a 16 squadre vengono create 8 partite
		assertEquals(8, t.getPartite().size());
		System.out.println(t.getCalendario().toString());
		//aggiungiamo i risultati del primo girone
		for (Partita p: t.getPartiteNonGiocate()) {
			ArrayList<Gol> listaGol =new ArrayList<>();
			listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
			listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
			listaGol.add(new Gol(p.getSquadra2(), p.getSquadra2().getListaGiocatori().get(0)));
			t.addRisultatoPartita(p, listaGol);
		}
		System.out.println(t.getCalendario().toString());
		//Essendo terminato il girone ne � stato inizializzato uno nuovo di quarti di finale a 4 partite
		assertEquals(12, t.getPartite().size());	
	}
}