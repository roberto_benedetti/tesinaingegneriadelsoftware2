package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import controller.CalcoloTorneiVinti;
import controller.CalcoloVittorieSquadra;
import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Torneo;

class TestCartellini {

	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		//ADD RISULTATI
		Partita p = t.getPartiteNonGiocate().get(0);
		//Test doppia ammonizione
		t.ammonisciGiocatore(p, p.getSquadra1().getListaGiocatori().get(0));
		t.ammonisciGiocatore(p, p.getSquadra1().getListaGiocatori().get(0));
		//Test espulsione
		t.espelliGiocatore(p, p.getSquadra1().getListaGiocatori().get(1));
	}
}

