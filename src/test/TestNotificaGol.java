package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.junit.jupiter.api.Test;

import controller.NotificaGol;
import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Squadra;
import model.Torneo;

class TestNotificaGol {

	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		t.addOsservatore(new NotificaGol());
		//ADD RISULTATI
		Partita p = t.getPartiteNonGiocate().get(0);
		ArrayList<Gol> listaGol =new ArrayList<>();
		listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
		listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
		listaGol.add(new Gol(p.getSquadra2(), p.getSquadra2().getListaGiocatori().get(0)));
		t.addRisultatoPartita(p, listaGol);
	}
}