package test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;

import org.h2.tools.DeleteDbFiles;

import controller.TorneoEliminazioneDiretta;
import db.InizializzatoreDB;
import model.GestoreTorneo;
import model.Giocatore;
import model.Squadra;
import model.Torneo;
import model.TorneoStandard;

public class Inizializzatore {
	// Inizializza un torneo a 16 squadre
	public GestoreTorneo InizializzaTorneo() throws SQLException {
        DeleteDbFiles.execute("~", "tesina_ingsw", true);
        InizializzatoreDB.dropDB();
        InizializzatoreDB db = new InizializzatoreDB();
		GestoreTorneo gestore = new GestoreTorneo();
		ArrayList<Giocatore> giocatori = new ArrayList<>(); 
		ArrayList<Squadra> squadre= new ArrayList<>(); 
		for (int i=0;i<48;i++) {
			Giocatore g = new Giocatore("NOME:"+i, "COGNOME", new GregorianCalendar(1993,6,28));
			giocatori.add(g);
		}
		for (int i=0;i<16;i++) {
			ArrayList<Giocatore> temp = new ArrayList<>();
			for(int j=0;j<3;j++) {
				temp.add(giocatori.remove(0));
			}
			squadre.add(new Squadra("Squadra:"+i, temp));
		}
		Torneo t = new TorneoStandard("Coppa", squadre, new TorneoEliminazioneDiretta(),gestore);
		gestore.addTorneo(t);
		return gestore;
	}
}
