package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.junit.jupiter.api.Test;

import controller.CalcoloTorneiVinti;
import controller.CalcoloVittorieSquadra;
import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Squadra;
import model.Torneo;

class TestPremi {

	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		//ADD RISULTATI
		while(!t.isTorneoFinito()) {
				for (Partita p: t.getPartiteNonGiocate()) {
					ArrayList<Gol> listaGol =new ArrayList<>();
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra2(), p.getSquadra2().getListaGiocatori().get(0)));
					t.addRisultatoPartita(p, listaGol);
				}	
		}
		System.out.println("Calendario Aggiornato:\n"+t.getCalendario().toString());
		LinkedHashMap<Squadra, String> classifica=t.generaClassifica();
		/*A Torneo finito possiamo ora verificare la classifica con i premi*/
		System.out.println(t.stampaClassifica());
		//Verifica che la squadra vincitrice sia quella giusta
		assertEquals(t.getListaSquadre().get(0), classifica.keySet().toArray()[0]);
		//Verifica la seconda classificata
		assertEquals(t.getListaSquadre().get(8), classifica.keySet().toArray()[1]);
		//Verifica la terza classificata
		assertEquals(t.getListaSquadre().get(4), classifica.keySet().toArray()[2]);

		//Verifica che i premi siano quelli giusti
		assertEquals("PREMIO STANDARD 1# POSIZIONE", classifica.get(t.getListaSquadre().get(0)));
		assertEquals("PREMIO STANDARD 2# POSIZIONE", classifica.get(t.getListaSquadre().get(8)));
		assertEquals("PREMIO STANDARD 3# POSIZIONE", classifica.get(t.getListaSquadre().get(4)));
		}
}
