package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import controller.CalcoloGol;
import controller.CalcoloPresenze;
import controller.CalcoloVittorieGiocatore;
import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Torneo;

class TestStatisticaGiocatore {

	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		//ADD RISULTATI
		while(!t.isTorneoFinito()) {
				for (Partita p: t.getPartiteNonGiocate()) {
					ArrayList<Gol> listaGol =new ArrayList<>();
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra2(), p.getSquadra2().getListaGiocatori().get(0)));
					t.addRisultatoPartita(p, listaGol);
				}
		}
		System.out.println("Calendario Aggiornato:\n"+t.getCalendario().toString());
		/*A questo punto calcoliamo i gol del giocatore della squadra vincente del torneo.
		Siccome � stato sempre lui a segnare il numero di gol sar� numPartite*2
		*/
		int gol=gestore.calcolaStatisticaGiocatore(new CalcoloGol(gestore), t.getListaSquadre().get(12).getListaGiocatori().get(0));
		assertEquals(6, gol);
		/*Testiamo ora il numero di partite vinte*/
		int vittorie=gestore.calcolaStatisticaGiocatore(new CalcoloVittorieGiocatore(gestore), t.getListaSquadre().get(12).getListaGiocatori().get(0));
		assertEquals(2, vittorie);
		/*Testiamo ora il numero di presenze allo stesso modo*/
		int presenze=gestore.calcolaStatisticaGiocatore(new CalcoloPresenze(gestore), t.getListaSquadre().get(12).getListaGiocatori().get(0));
		assertEquals(4, presenze);
	}
}