package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import controller.CalcoloGol;
import controller.CalcoloPresenze;
import controller.CalcoloTorneiVinti;
import controller.CalcoloVittorieSquadra;
import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Torneo;

class TestStatisticaSquadra {
	
	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		//ADD RISULTATI
		while(!t.isTorneoFinito()) {
				for (Partita p: t.getPartiteNonGiocate()) {
					ArrayList<Gol> listaGol =new ArrayList<>();
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra2(), p.getSquadra2().getListaGiocatori().get(0)));
					t.addRisultatoPartita(p, listaGol);
				}
		}
		System.out.println("Calendario Aggiornato:\n"+t.getCalendario().toString());
		/*A Torneo finito possiamo ora verificare che il numero di tornei vinti dalla squadra 0 � 1
		*/
		int torneiVinti=gestore.calcolaStatisticaSquadra(new CalcoloTorneiVinti(gestore), t.getListaSquadre().get(0));
		assertEquals(1, torneiVinti);
		/*Testiamo ora il numero di singole vittorie allo stesso modo*/
		int partiteVinte=gestore.calcolaStatisticaSquadra(new CalcoloVittorieSquadra(gestore), t.getListaSquadre().get(0));
		assertEquals(4, partiteVinte);
	}
}