package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import db.DBGestoreTorneo;
import db.DBGiocatore;
import db.DBGirone;
import db.DBGol;
import db.DBPartecipazione;
import db.DBPartita;
import db.DBSquadra;
import db.DBTorneo;
import model.GestoreTorneo;
import model.Gol;
import model.Partita;
import model.Torneo;

class TestDB {

	@Test
	void test() throws SQLException {
		GestoreTorneo gestore  = new Inizializzatore().InizializzaTorneo();
		Torneo t = gestore.getListaTornei().get(0);
		//ADD RISULTATI
		while(!t.isTorneoFinito()) {
				for (Partita p: t.getPartiteNonGiocate()) {
					ArrayList<Gol> listaGol =new ArrayList<>();
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra1(), p.getSquadra1().getListaGiocatori().get(0)));
					listaGol.add(new Gol(p.getSquadra2(), p.getSquadra2().getListaGiocatori().get(0)));
					t.addRisultatoPartita(p, listaGol);
				}
		}
		DBGestoreTorneo.select();
		DBGiocatore.select();
		DBPartecipazione.select();
		DBPartita.select();
		DBGirone.select();
		DBGol.select();
		DBSquadra.select();
		DBTorneo.select();
	}
}