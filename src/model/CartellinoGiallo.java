package model;

public class CartellinoGiallo extends StatoCartellino{

	public CartellinoGiallo(SchedaGiocatore scheda) {
		super(scheda);
		System.out.println("Ammonito! "+scheda.getGiocatore().getNome());
	}

	@Override
	public void ammonisci() {
		this.scheda.setCartellino(new CartellinoRosso(scheda));
	}


}
