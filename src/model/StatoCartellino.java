package model;

public abstract class StatoCartellino {
	protected SchedaGiocatore scheda;
	public StatoCartellino(SchedaGiocatore scheda) {
		this.scheda=scheda;
	}
	public void resetta() {
		this.scheda.setCartellino(new CartellinoNeutro(scheda));
	}	
	public void espelli() {
		this.scheda.setCartellino(new CartellinoRosso(scheda));
	}
	public abstract void ammonisci();



	

}
