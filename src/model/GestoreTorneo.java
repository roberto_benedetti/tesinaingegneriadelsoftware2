package model;

import java.sql.SQLException;
import java.util.ArrayList;

import controller.AssegnazionePremi3;
import controller.CalcoloStatisticaGiocatore;
import controller.CalcoloStatisticaSquadra;
import db.DBGestoreTorneo;
import db.DBTorneo;

public class GestoreTorneo {
	private ArrayList<Torneo> listaTornei;
	private static int Gid=0;
	private int id;
		

	public GestoreTorneo() throws SQLException {
		super();
		this.listaTornei = new ArrayList<>();
		DBGestoreTorneo a = new DBGestoreTorneo();
		this.id=getNewId();
		a.insert(this.id);

	}
	public static int getNewId() {
		return Gid++;
	}
	public ArrayList<Torneo> getListaTornei() {
		return listaTornei;
	}

	public void addTorneo(Torneo t) throws SQLException {
		this.listaTornei.add(t);

	}
	
	//METODO DEL GESTORE PER IL CALCOLO DELLE STATISTICHE
	public int calcolaStatisticaSquadra(CalcoloStatisticaSquadra c, Squadra s) {
		return c.calcola(s);
	}
	public int calcolaStatisticaGiocatore(CalcoloStatisticaGiocatore c,Giocatore g) {
		return c.calcola(g);
	}

	public int getId() {
		// TODO Auto-generated method stub
		return id;
	}
	
}
