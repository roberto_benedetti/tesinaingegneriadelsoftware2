package model;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Stack;

import controller.AssegnazionePremi3;
import controller.IStrategiaTorneo;
import controller.OsservatorePartita;
import controller.AssegnazionePremi;
import controller.TorneoEliminazioneDiretta;
import db.DBGiocatore;
import db.DBGol;
import db.DBPartecipazione;
import db.DBPartita;
import db.DBTorneo;

public abstract class Torneo{
	private String nome;
	private ArrayList<Squadra> listaSquadre;
	private CalendarioPartite calendario;
	private boolean isTorneoFinito;
	private AssegnazionePremi premi;
	private ArrayList<OsservatorePartita> listaOsservatori;
	private IStrategiaTorneo s;
	protected GestoreTorneo g;
	public Torneo(String nome,ArrayList<Squadra> listaSquadre, IStrategiaTorneo s,GestoreTorneo g) throws SQLException {
		this.s=s;
		this.g=g;
		this.nome=nome;
		this.listaSquadre=listaSquadre;
		//SEZIONE DB
		DBTorneo a = new DBTorneo();
		a.insert(nome,g.getId(),this.getClass().getName(), s.getClass().getName());
		this.isTorneoFinito =false;
		this.premi=new AssegnazionePremi3();
		this.listaOsservatori = new ArrayList<>();
		this.calendario =new CalendarioPartite(listaSquadre, s, this);
		
		DBPartecipazione b = new DBPartecipazione();
		for(Squadra squadra:listaSquadre)
			b.insert(squadra.getNome(), this.nome, this.g.getId());

	}

	public GestoreTorneo getG() {
		return g;
	}
	public IStrategiaTorneo getS() {
		return s;
	}

	public void setPremi(AssegnazionePremi premi) {
		this.premi = premi;
	}

	public String getNome() {
		return nome;
	}
	public ArrayList<Squadra> getListaSquadre() {
		return listaSquadre;
	}
	public boolean isTorneoFinito() {
		return this.isTorneoFinito;
	}
	public CalendarioPartite getCalendario() {
		return calendario;
	}
	
	public void aggiornaStatoTorneo() {
		if(this.getPartiteNonGiocate().isEmpty()) {
			this.isTorneoFinito=true;
			System.out.println("Fine Torneo!");
		}	
	}
	public ArrayList<Partita> getPartite() {

		return this.calendario.getPartite();
	}

	public ArrayList<Partita> getPartiteNonGiocate() {
		ArrayList<Partita> partiteNonGiocate=new ArrayList<>();
		for(Partita p:this.getPartite())
			if(!p.isGiocata())
				partiteNonGiocate.add(p);
		return partiteNonGiocate;
	}
	
	public void addRisultatoPartita(Partita p,ArrayList<Gol> listaGol) throws SQLException {
		if(p.isGiocata()==false) {
			p.setListaGol(listaGol);
			for(Gol g:listaGol) {
				if(g.getSquadraSegnante()==p.getSquadra1())
					p.getRisultato()[0]++;
				if(g.getSquadraSegnante()==p.getSquadra2())
					p.getRisultato()[1]++;
			}
			p.setGiocata(true);
			p.getGirone().templateAggiorna();
			this.aggiornaStatoTorneo();
			this.notificaOsservatori(p);

			//SEZIONE DB
			DBPartita a = new DBPartita();
			a.insert(p.getId(),p.getGirone().getId(),p.getSquadra1().getNome(),p.getSquadra2().getNome(),p.getRisultato()[0],p.getRisultato()[1]);

			DBGol b= new DBGol();
			for(Gol g:listaGol) {
				b.insert(g.getId(), g.getSquadraSegnante().getNome(), g.getGiocatore().getId(), p.getId(), p.getGirone().getId());
			}
		}
	}

	public void ammonisciGiocatore(Partita p,Giocatore g) {
		ArrayList<Giocatore> listaGiocatori = new ArrayList<>();
		listaGiocatori.addAll(p.getSquadra1().getListaGiocatori());
		listaGiocatori.addAll(p.getSquadra2().getListaGiocatori());
		if(listaGiocatori.contains(g))
			g.getSchedaGiocatore().ammonisci();
		
		
	}
	public void espelliGiocatore(Partita p,Giocatore g) {
		ArrayList<Giocatore> listaGiocatori = new ArrayList<>();
		listaGiocatori.addAll(p.getSquadra1().getListaGiocatori());
		listaGiocatori.addAll(p.getSquadra2().getListaGiocatori());
		if(listaGiocatori.contains(g))
			g.getSchedaGiocatore().espelli();
	}	

//	public boolean isGold() {
//		return isGold;
//	}
//	public void notificaGol(Gol g) {
//		System.out.println("GOL REGISTRATO");
//	}
//	
	public LinkedHashMap<Squadra, String> generaClassifica(){
		LinkedHashMap<Squadra, String> classifica = new LinkedHashMap<>();

		if(isTorneoFinito) {
			Stack<Partita> pila = new Stack<>();
			pila.addAll(this.getPartite());
			int j=0;
			for(int i=0;i<2;i++) {
				j++;
				classifica.put(pila.peek().getVincitrice(),this.visita(this.premi, j));
				j++;
				classifica.put(pila.pop().getPerdente(),this.visita(this.premi, j));
			}
		}
		return classifica;
	}
	public String stampaClassifica() {
		String out="";
		int i=1;
		LinkedHashMap<Squadra, String> classifica = this.generaClassifica();
		for (Squadra s: classifica.keySet()) {
			out+=i+"#\t"+s.getNome()+"\t"+ classifica.get(s)+"\n";
			i++;
		}
		return out;
			
	}
	public abstract String visita(AssegnazionePremi op, int i);
	
	public void addOsservatore(OsservatorePartita o) {
		this.listaOsservatori.add(o);
	}
	public void notificaOsservatori(Partita p) {
		for(OsservatorePartita o:listaOsservatori)
			o.aggiorna(p);
	}
	
}