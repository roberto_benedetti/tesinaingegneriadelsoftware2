package model;

import java.util.ArrayList;

public class GironeFinale3 extends GironeAstratto{

	public GironeFinale3(ArrayList<Squadra> listaSquadre, Torneo t) {
		super(listaSquadre, t);
		this.nomeGirone = "GIRONE FINALE 3 POSTO";
	}

	@Override
	protected void addGironi() {		
	}

}
