package model;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Stack;

import controller.IStrategiaTorneo;
import controller.TorneoEliminazioneDiretta;
import db.DBGirone;


public class CalendarioPartite implements ElementoTorneo{
	private ArrayList<Squadra> listaSquadre;
	private ArrayList<GironeAstratto> listaGironi;
	private IStrategiaTorneo strategia;
	private Torneo t;

	
	public CalendarioPartite(ArrayList<Squadra> listaSquadre, IStrategiaTorneo strategia, Torneo t) throws SQLException {
		super();
		this.t=t;
		this.listaSquadre=listaSquadre;
		this.strategia = strategia;
		listaGironi= new ArrayList<>();
		listaGironi.addAll(strategia.inizializzaTorneo(this.listaSquadre,t));
		//SEZIONE DB
		DBGirone a = new DBGirone(); 
		for(GironeAstratto g:listaGironi) 
			a.insert2(g.getId(),t.getNome(), t.getG().getId(), g.getNomeGirone());
	}
		
	public Torneo getT() {
		return t;
	}


	public ArrayList<GironeAstratto> getListaGironi() {
		return listaGironi;
	}

	public String toString() {
		String out="CALENDARIO TORNEO\n\t";
		for (GironeAstratto g:listaGironi)
			out+=g.toString()+"\n\t";
		return out;
	}


	@Override
	public ArrayList<Partita> getPartite() {
		ArrayList<Partita> l = new ArrayList<>();
		for (GironeAstratto g:listaGironi)
			l.addAll(g.getPartite());
		return l;
	}



	
}
