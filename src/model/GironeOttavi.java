package model;

import java.util.ArrayList;

public class GironeOttavi extends GironeAstratto {

	public GironeOttavi(ArrayList<Squadra> listaSquadre,Torneo t) {
		super(listaSquadre,t);
		this.nomeGirone="OTTAVI DI FINALE";
	}

	@Override
	protected void addGironi() {
		this.figli.add(new GironeQuarti(this.getVincitrici(),t));
		
	}


}
