package model;
import java.sql.SQLException;
import java.util.ArrayList;

import db.DBGiocatore;
import db.DBSquadra;

public class Squadra {
	private String nome;
	private ArrayList<Giocatore> listaGiocatori;
	
	public Squadra(String nome, ArrayList<Giocatore> listaGiocatori) throws SQLException {
		super();
		this.nome = nome;
		this.listaGiocatori = listaGiocatori;
		//SEZIONE DB
		DBSquadra a = new DBSquadra();
		a.insert(this.nome);
		DBGiocatore b = new DBGiocatore();
		for(Giocatore g:listaGiocatori) {
			b.insert(g.getId(), g.getNome(), g.getCognome(), g.getDataNasciata(), this.nome, g.getSchedaGiocatore().getCartellino().getClass().getName());
		}
	}

	public String getNome() {
		return nome;
	}

	public ArrayList<Giocatore> getListaGiocatori() {
		return listaGiocatori;
	}
	
}
