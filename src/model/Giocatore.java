package model;
import java.util.GregorianCalendar;

public class Giocatore {
	private String nome;
	private String cognome;
	private GregorianCalendar dataNasciata;
	private SchedaGiocatore scheda;
	private static int Gid=0;
	private int id;
	public Giocatore(String nome, String cognome, GregorianCalendar dataNasciata) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.dataNasciata = dataNasciata;
		this.scheda=new SchedaGiocatore(this);
		this.id=getNewId();
	}
	public static int getNewId() {
		return Gid++;
	}

	public int getId() {
		return id;
	}
	public String getNome() {
		return nome;
	}
	public String getCognome() {
		return cognome;
	}
	public GregorianCalendar getDataNasciata() {
		return dataNasciata;
	}
	public SchedaGiocatore getSchedaGiocatore() {
		return this.scheda;
	}
	@Override
	public String toString() {
		return "Giocatore [nome=" + nome + ", cognome=" + cognome + ", dataNasciata=" + dataNasciata.getTime() + "]";
	}
	
	
	
	

}
