package model;

import java.sql.SQLException;
import java.util.ArrayList;

import controller.AssegnazionePremi;
import controller.IStrategiaTorneo;

public class TorneoStandard extends Torneo{

	public TorneoStandard(String nome,ArrayList<Squadra> listaSquadre, IStrategiaTorneo s,GestoreTorneo g) throws SQLException {
		super(nome, listaSquadre,s,g);
	}

	@Override
	public String visita(AssegnazionePremi op, int i) {
		return op.generaPremio(this,i);
	}

}
