package model;

import java.util.ArrayList;

public class GironeFinale extends GironeAstratto {

	public GironeFinale(ArrayList<Squadra> listaSquadre,Torneo t) {
		super(listaSquadre,t);
		this.nomeGirone="GIRONE DI FINALE";
	}

	@Override
	protected void addGironi() {
	}
}
