package model;

import controller.CalcoloGol;
import controller.CalcoloPresenze;
import controller.CalcoloStatisticaGiocatore;

public class SchedaGiocatore {
	private Giocatore giocatore;
	private StatoCartellino cartellino;
	
	public SchedaGiocatore(Giocatore giocatore) {
		super();
		this.giocatore = giocatore;

		this.cartellino= new CartellinoNeutro(this);
	}
	
	public Giocatore getGiocatore() {
		return giocatore;
	}

	public int getStatistica(CalcoloStatisticaGiocatore c) {
		return c.calcola(this.giocatore);
	}
	
	public StatoCartellino getCartellino() {
		return cartellino;
	}
	public void setCartellino(StatoCartellino c) {
		this.cartellino=c;
	}
	public void ammonisci() {
		this.cartellino.ammonisci();
	}
	public void espelli() {
		this.cartellino.espelli();
	}
	public void resettaCartellino() {
		this.cartellino.resetta();
	}
	
	

}
