package model;

import java.util.ArrayList;

public class GironeQuarti extends GironeAstratto {

	public GironeQuarti(ArrayList<Squadra> listaSquadre,Torneo t) {
		super(listaSquadre,t);
		this.nomeGirone="QUARTI DI FINALE";
	}

	@Override
	public void addGironi() {
		this.figli.add(new GironeSemifinale(this.getVincitrici(),t));
 	}



}
