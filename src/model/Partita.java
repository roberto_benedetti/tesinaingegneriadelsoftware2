package model;
import java.util.ArrayList;

public class Partita {
	private Squadra squadra1;
	private Squadra squadra2;
	private ArrayList<Gol> listaGol;
	private int[] risultato;
	private boolean isGiocata;
	private GironeAstratto girone;
	private static int Gid=0;
	private int id;
	public Partita(Squadra squadra1, Squadra squadra2,GironeAstratto girone) {
		super();
		this.squadra1 = squadra1;
		this.squadra2 = squadra2;
		this.listaGol = new ArrayList<>();
		int[] risultato = {0,0};
		this.risultato =risultato;
		this.isGiocata=false;
		this.girone=girone;
		this.id=getNewId();

		for(Giocatore g:squadra1.getListaGiocatori())
			g.getSchedaGiocatore().resettaCartellino();
		for(Giocatore g:squadra2.getListaGiocatori())
			g.getSchedaGiocatore().resettaCartellino();
	}
	public static int getNewId() {
		return Gid++;
	}
	
	public int getId() {
		return id;
	}


	public void setListaGol(ArrayList<Gol> listaGol) {
			this.listaGol = listaGol;
		}


	public GironeAstratto getGirone() {
		return girone;
	}


	public void setGiocata(boolean isGiocata) {
		this.isGiocata = isGiocata;
	}


	//	void addRisultato(ArrayList<Gol> listaGol) {
//		if(isGiocata==false) {
//			this.listaGol=listaGol;
//			for(Gol g:listaGol) {
//				if(g.getSquadraSegnante()==squadra1)
//					risultato[0]++;
//				if(g.getSquadraSegnante()==squadra2)
//				risultato[1]++;
//			}
//			this.isGiocata=true;
//			this.girone.templateAggiorna();
//		}
//			
//	}
//	void addCartellino(Giocatore g, int colore) {
//		ArrayList<Giocatore> listaGiocatori = new ArrayList<>();
//		listaGiocatori.addAll(squadra1.getListaGiocatori());
//		listaGiocatori.addAll(squadra2.getListaGiocatori());
//		if(listaGiocatori.contains(g)){
//			g.getSchedaGiocatore().setStatoCartellino(colore);
//		}
//	}
//	void ammonisciGiocatore(Giocatore g) {
//
//		}
//	}
//	void espelliGiocatore(Giocatore g) {
//		ArrayList<Giocatore> listaGiocatori = new ArrayList<>();
//		listaGiocatori.addAll(squadra1.getListaGiocatori());
//		listaGiocatori.addAll(squadra2.getListaGiocatori());
//		if(listaGiocatori.contains(g)){
//			g.getSchedaGiocatore().espelli();
//		}
//	}
	
	public Squadra getSquadra1() {
		return squadra1;
	}
	public Squadra getSquadra2() {
		return squadra2;
	}

	public ArrayList<Gol> getListaGol() {
		return listaGol;
	}

	public int[] getRisultato() {
		return risultato;
	}

	public boolean isGiocata() {
		return isGiocata;
	}
	@Override
	public String toString() {
		return "["+squadra1.getNome()+" "+risultato[0]+"-"+risultato[1]+" "+squadra2.getNome()+"] Giocata:("+isGiocata+")";

	}
	public Squadra getVincitrice() {
		if(!isGiocata) {
			throw new IllegalArgumentException("Partita non ancora giocata");
		}else {
			if (risultato[0]>risultato[1])
				return this.squadra1;
			else
				return this.squadra2;
		}
	}
	public Squadra getPerdente() {
		if(!isGiocata) {
			throw new IllegalArgumentException("Partita non ancora giocata");
			
		}else {
			if (risultato[0]<risultato[1])
				return this.squadra1;
			else
				return this.squadra2;
		}
	}


	
	
	
	
	
}
