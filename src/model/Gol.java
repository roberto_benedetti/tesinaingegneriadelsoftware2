package model;

public class Gol {
	private Squadra squadraSegnante;
	private Giocatore giocatore;
	private static int Gid=0;
	private int id;
	
	public Gol(Squadra squadraSegnante, Giocatore giocatore) {
		super();
		this.squadraSegnante = squadraSegnante;
		this.giocatore = giocatore;
		this.id=getNewId();
	}
	public static int getNewId() {
		return Gid++;
	}
	public int getId() {
		return id;
	}

	public Squadra getSquadraSegnante() {
		return squadraSegnante;
	}

	public Giocatore getGiocatore() {
		return giocatore;
	}
	

}
