package model;

import java.util.ArrayList;

public class GironeSemifinale extends GironeAstratto {

	public GironeSemifinale(ArrayList<Squadra> listaSquadre,Torneo t) {
		super(listaSquadre,t);
		this.nomeGirone = "GIRONE DI SEMIFINALE";
	}

	@Override
	protected void addGironi() {
		this.figli.add(new GironeFinale3(this.getPerdenti(),t));
		this.figli.add(new GironeFinale(this.getVincitrici(),t));
		
	}


}
