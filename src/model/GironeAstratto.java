package model;

import java.sql.SQLException;
import java.util.ArrayList;

import controller.IStrategiaTorneo;
import controller.TorneoEliminazioneDiretta;
import db.DBGirone;

public abstract class GironeAstratto implements ElementoTorneo{
	protected ArrayList<Squadra> listaSquadre;
	protected ArrayList<Partita> listaPartiteGirone;
	protected ArrayList<GironeAstratto> figli;
	protected String nomeGirone="";
	protected Torneo t;
	private static int Gid=0;
	private int id;
			
	public GironeAstratto(ArrayList<Squadra> listaSquadre,Torneo t) {
		this.t=t;
		this.listaSquadre=listaSquadre;
		this.figli=new ArrayList<>();
		this.listaPartiteGirone = this.costruisciPartite();
		this.id=getNewId();
	}
	public static int getNewId() {
		return Gid++;
	}
	public int getId() {
		return id;
	}
	protected int getRandom() {
		Double id = (Math.random()*10000);
		return id.intValue();
	}
	
	public Torneo getT() {
		return t;
	}

	public String getNomeGirone() {
		return nomeGirone;
	}

	//Metodo comportamento default di un girone
	protected ArrayList<Partita> costruisciPartite() {
		ArrayList<Partita> listaPartite = new ArrayList<>();
		for(int i=0;i<listaSquadre.size();i+=2)
			listaPartite.add(new Partita(this.listaSquadre.get(i), this.listaSquadre.get(i+1),this));
		return listaPartite;
	}
	@Override
	public ArrayList<Partita> getPartite(){
		ArrayList<Partita> l = new ArrayList<>();
		l.addAll(this.getPartiteGirone());
		for(GironeAstratto g: figli)
			l.addAll(g.getPartite());
		return l;
	}
	
	public ArrayList<Partita> getPartiteGirone() {
		return listaPartiteGirone;
	}

	protected ArrayList<Squadra> getVincitrici(){
		ArrayList<Squadra> list = new ArrayList<>();
		for (Partita p:this.listaPartiteGirone)
			if(p.isGiocata())
				list.add(p.getVincitrice());
		return list;
	}
	protected ArrayList<Squadra> getPerdenti(){
		ArrayList<Squadra> list = new ArrayList<>();
		for (Partita p:this.listaPartiteGirone)
			if(p.isGiocata())
				list.add(p.getPerdente());
		return list;
	}

	public void templateAggiorna() throws SQLException {
		if(this.checkFineGirone()) {
			this.addGironi();
			DBGirone a = new DBGirone(); 
			for(GironeAstratto g:figli) {
				g.templateAggiorna();
				//SEZIONE DB
//				System.out.println("DEBUG INSERIMENTO:\nID:"+g.getId()+" TNOME:"+t.getNome()+" GID:"+t.getG().getId()+" NOMEG:"+g.getNomeGirone()+" PNOMEG:"+this.nomeGirone);
//				a.insert(g.getId(),t.getNome(), t.getG().getId(), g.getNomeGirone(), this.nomeGirone);
				a.insert(g.getId(),this.t.getNome(),t.getG().getId(),g.getNomeGirone(),this.id);
			}
		}
	}
	protected boolean checkFineGirone() {
		for(Partita p:listaPartiteGirone)
			if(!p.isGiocata())
				return false;
		return true;
	}
	public String toString() {
		String out =this.nomeGirone+"\n\t";
		for (Partita p:listaPartiteGirone)
			out+=p.toString()+"\n\t";
		for(GironeAstratto g:figli)
			out+=g.toString()+"\n\t";
		return out;
	}
	protected abstract void addGironi();
}
