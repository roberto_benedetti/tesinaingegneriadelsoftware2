package model;

public class CartellinoNeutro extends StatoCartellino{

	public CartellinoNeutro(SchedaGiocatore scheda) {
		super(scheda);
	}

	@Override
	public void ammonisci() {
		this.scheda.setCartellino(new CartellinoGiallo(scheda));
		
	}


}
