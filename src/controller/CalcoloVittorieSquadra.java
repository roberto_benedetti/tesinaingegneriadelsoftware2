package controller;

import model.GestoreTorneo;
import model.Partita;
import model.Squadra;
import model.Torneo;

public class CalcoloVittorieSquadra extends ACalcoloStatisticaSquadra {

	public CalcoloVittorieSquadra(GestoreTorneo g) {
		super(g);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int calcola(Squadra s) {
		int num=0;
		for (Torneo t: g.getListaTornei())
			for(Partita p:t.getPartite())
				if(p.isGiocata())
					if(p.getVincitrice()==s)
						num++;
		return num;
	}

}
