package controller;

import model.GestoreTorneo;
import model.Squadra;
import model.Torneo;

public class CalcoloTorneiVinti extends ACalcoloStatisticaSquadra {

	public CalcoloTorneiVinti(GestoreTorneo g) {
		super(g);
	}

	@Override
	public int calcola(Squadra s) {
		int num=0;
		for (Torneo t:g.getListaTornei())
			if(t.isTorneoFinito())
				if(t.getPartite().get(t.getPartite().size()-1).getVincitrice()==s)
					num++;
		return num;
	}

}
