package controller;

import model.Gol;
import model.Partita;

public class NotificaGol implements OsservatorePartita {

	@Override
	public void aggiorna(Partita p) {
		for(Gol g:p.getListaGol())
			System.out.println("Registrato GOL!\tSquadra: "+g.getSquadraSegnante().getNome()+"\t"+"Giocatore: "+g.getGiocatore().getNome());
	}

}
