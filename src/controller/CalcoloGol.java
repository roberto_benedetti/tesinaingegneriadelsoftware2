package controller;

import model.GestoreTorneo;
import model.Giocatore;
import model.Gol;
import model.Partita;
import model.SchedaGiocatore;
import model.Torneo;

public class CalcoloGol extends ACalcoloStatisticaGiocatore{

	public CalcoloGol(GestoreTorneo gestore) {
		super(gestore);
	}

	@Override
	public int calcola(Giocatore g) {
		int num=0;
		for(Torneo t:gestore.getListaTornei())
			for(Partita p:t.getPartite())
				if(p.isGiocata())
					for(Gol gol:p.getListaGol())
						if(gol.getGiocatore()==g)
							num++;
		return num;
	}

}
