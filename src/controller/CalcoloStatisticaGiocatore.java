package controller;

import model.Giocatore;

public interface CalcoloStatisticaGiocatore {
	public int calcola(Giocatore g);
}
