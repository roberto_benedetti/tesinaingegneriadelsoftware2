package controller;

import model.GestoreTorneo;
import model.Giocatore;
import model.Partita;
import model.SchedaGiocatore;
import model.Torneo;

public class CalcoloVittorieGiocatore extends ACalcoloStatisticaGiocatore{

	public CalcoloVittorieGiocatore(GestoreTorneo gestore) {
		super(gestore);
	}

	@Override
	public int calcola(Giocatore g) {
		int num=0;
		for(Torneo t:gestore.getListaTornei())
			for(Partita p:t.getPartite())
				if(p.isGiocata())
					if(p.getVincitrice().getListaGiocatori().contains(g))
						num++;
		return num;
	}
}
