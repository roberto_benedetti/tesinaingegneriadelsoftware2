package controller;

import model.GestoreTorneo;
import model.Giocatore;
import model.Gol;
import model.Partita;
import model.SchedaGiocatore;
import model.Torneo;

public class CalcoloPresenze extends ACalcoloStatisticaGiocatore{

	public CalcoloPresenze(GestoreTorneo gestore) {
		super(gestore);
	}
	
	@Override
	public int calcola(Giocatore g) {
		int num=0;
		for(Torneo t:gestore.getListaTornei())
			for(Partita p:t.getPartite())
				if(p.isGiocata())
					if(p.getSquadra1().getListaGiocatori().contains(g) || p.getSquadra2().getListaGiocatori().contains(g))
						num++;
		return num;
	}
}
