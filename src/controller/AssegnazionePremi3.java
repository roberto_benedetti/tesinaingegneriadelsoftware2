package controller;

import model.TorneoGold;
import model.TorneoStandard;

public class AssegnazionePremi3 implements AssegnazionePremi{

	@Override
	public String generaPremio(TorneoGold t, int i) {
		if(i<4)
			return "PREMIO GOLD "+i+"# POSIZIONE";
		return "";
	}

	@Override
	public String generaPremio(TorneoStandard t, int i) {
		if(i<4)
			return "PREMIO STANDARD "+i+"# POSIZIONE";
		return "";

	}

	

}
