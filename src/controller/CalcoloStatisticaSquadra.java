package controller;

import model.Squadra;

public interface CalcoloStatisticaSquadra {
	public int calcola(Squadra s);

}
