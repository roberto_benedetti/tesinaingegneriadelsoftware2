package controller;

import model.TorneoGold;
import model.TorneoStandard;

public interface AssegnazionePremi {
	public String generaPremio(TorneoGold t, int i);
	public String generaPremio(TorneoStandard t, int i);
}
