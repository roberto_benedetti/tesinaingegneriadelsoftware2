package controller;

import java.util.ArrayList;

import model.*;

public class TorneoEliminazioneDiretta implements IStrategiaTorneo{
	@Override
	public ArrayList<GironeAstratto> inizializzaTorneo(ArrayList<Squadra> l,Torneo t) {
		ArrayList<GironeAstratto> out = new ArrayList<>();
		switch(l.size()) {
		case 16:
			out.add(new GironeOttavi(l,t));
			break;
		case 8:
			out.add(new GironeQuarti(l,t));
			break;
		case 4:
			out.add(new GironeSemifinale(l,t)); 
			break;
		case 2:
			out.add(new GironeFinale(l,t)); 
			break; 
		default:
			System.out.println("Numero squadre non valido");
		}
		return out;
	}

}
