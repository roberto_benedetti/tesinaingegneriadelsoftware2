package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.experimental.theories.Theories;

public class DBPartita implements Tabella{
	
	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBPartita` (" + 
				"  `idPartita` INT NOT NULL," + 
				"  `idGirone` INT NOT NULL," + 
				"  `NomeSquadra1` VARCHAR(255) NOT NULL," + 
				"  `NomeSquadra2` VARCHAR(255) NOT NULL," + 
				"  `gol1` INT NULL," + 
				"  `gol2` INT NULL," + 
				"  PRIMARY KEY (`idGirone`, `idPartita`)," + 
//				"  INDEX `fk_DBPartita_DBSquadra1_idx` (`NomeSquadra2` ASC)," + 
//				"  INDEX `fk_DBPartita_DBSquadra2_idx` (`NomeSquadra1` ASC)," + 
//				"  INDEX `fk_DBPartita_DBGirone1_idx` (`idGirone` ASC)," + 
				"  CONSTRAINT `fk_DBPartita_DBSquadra1`" + 
				"    FOREIGN KEY (`NomeSquadra2`)" + 
				"    REFERENCES `Calcetto`.`DBSquadra` (`nomeSquadra`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION," + 
				"  CONSTRAINT `fk_DBPartita_DBSquadra2`" + 
				"    FOREIGN KEY (`NomeSquadra1`)" + 
				"    REFERENCES `Calcetto`.`DBSquadra` (`nomeSquadra`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION," + 
				"  CONSTRAINT `fk_DBPartita_DBGirone1`" + 
				"    FOREIGN KEY (`idGirone`)" + 
				"    REFERENCES `Calcetto`.`DBGirone` (`id`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION)" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();
	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
		
	}
	public static void insert(int idPartita, int idGirone,String NomeSquadra1,String NomeSquadra2,int gol1,int gol2) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
 
        String InsertQuery = "INSERT INTO DBPartita" + "(idPartita,idGirone,NomeSquadra1,NomeSquadra2,gol1,gol2) values" + "(?,?,?,?,?,?)";
        try {
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setInt(1,  idPartita);
            insertPreparedStatement.setInt(2, idGirone);
            insertPreparedStatement.setString(3, NomeSquadra1);
            insertPreparedStatement.setString(4, NomeSquadra2);
            insertPreparedStatement.setInt(5, gol1);
            insertPreparedStatement.setInt(6, gol2);

            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella Partita, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBPARTITA";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBPARTITA:");
            while (rs.next()) {
            	 System.out.println("\tidPartita: "+rs.getInt("idPartita")+"\tidGirone: "+rs.getInt("idGirone")+"\tNomeSquadra1: "+rs.getString("NomeSquadra1")+
            			 "\tNomeSquadra2: "+rs.getString("NomeSquadra2")+"\tgol1: "+rs.getInt("gol1")+"\tidgol2: "+rs.getInt("gol2"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
		
	}

}

