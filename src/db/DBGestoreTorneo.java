package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DBGestoreTorneo implements Tabella {

	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBGestoreTorneo` (" + 
				"  `idGestoreTorneo` INT NOT NULL," + 
				"  PRIMARY KEY (`idGestoreTorneo`))" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();
	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
		
	}
	public static void insert(int i) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String InsertQuery = "INSERT INTO DBGestoreTorneo" + "(idGestoreTorneo) values" + "(?)";
        try {
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setInt(1, i);
            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}

	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBGESTORETORNEO";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBGESTORETORNEO:");
            while (rs.next()) {
            	 System.out.println("\tidGestoreTorneo: "+rs.getInt("idGestoreTorneo"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
}
