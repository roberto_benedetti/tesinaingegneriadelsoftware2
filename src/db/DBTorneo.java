package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBTorneo implements Tabella {
	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBTorneo` (" + 
				"  `nomeTorneo` VARCHAR(255) NOT NULL," + 
				"  `idGestoreTorneo` INT NOT NULL," + 
				"  `tipoTorneo` VARCHAR(45) NULL," + 
				"  `strategia` VARCHAR(255) NULL," + 
				"  PRIMARY KEY (`nomeTorneo`, `idGestoreTorneo`)," + 
//				"  INDEX `fk_DBTorneo_DBGestoreTorneo_idx` (`idGestoreTorneo` ASC)," + 
				"  CONSTRAINT `fk_DBTorneo_DBGestoreTorneo`" + 
				"    FOREIGN KEY (`idGestoreTorneo`)" + 
				"    REFERENCES `Calcetto`.`DBGestoreTorneo` (`idGestoreTorneo`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION)" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();
	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
		
	}
	public static void insert(String nomeTorneo, int idGestore, String tipoTorneo, String strategia) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String InsertQuery = "INSERT INTO DBTorneo" + "(nomeTorneo,idGestoreTorneo,tipoTorneo,strategia) values" + "(?,?,?,?)";
        try {
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setString(1, nomeTorneo);
            insertPreparedStatement.setInt(2, idGestore);
            insertPreparedStatement.setString(3, tipoTorneo);
            insertPreparedStatement.setString(4, strategia);

            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBTORNEO";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBTORNEO:");
            while (rs.next()) {
            	 System.out.println("\tnomeTorneo: "+rs.getString("nomeTorneo")+"\tidGestoreTorneo: "+rs.getInt("idGestoreTorneo")+"\ttipoTorneo: "+rs.getString("tipoTorneo")+
            			 "\tstrategia: "+rs.getString("strategia"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
		
	}

}



