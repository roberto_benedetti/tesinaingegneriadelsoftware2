package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connessione {
	
    private static final String DB_DRIVER = "org.h2.Driver";
    private static final String DB_CONNECTION = "jdbc:h2:tcp://localhost/~/tesina_ingsw;"+"INIT=CREATE SCHEMA IF NOT EXISTS Calcetto\\;" +"SET SCHEMA Calcetto";
    private static final String DB_USER = "SA";
    private static final String DB_PASSWORD = "";

    
    public static Connection getDBConnection() {
	    Connection dbConnection = null;
	    try {
	        Class.forName(DB_DRIVER);
	    } catch (ClassNotFoundException e) {
	        System.out.println(e.getMessage());
	    }
	    try {
	        dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,
	                DB_PASSWORD);
	        return dbConnection;
	    } catch (SQLException e) {
	        System.out.println(e.getMessage());
	    }
	    return dbConnection;
	}
}