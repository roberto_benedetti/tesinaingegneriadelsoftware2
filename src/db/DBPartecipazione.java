package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBPartecipazione implements Tabella{
	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBPartecipazione` (" + 
				"  `nomeSquadra` VARCHAR(255) NOT NULL," + 
				"  `nomeTorneo` VARCHAR(255) NOT NULL," + 
				"  `idGestoreTorneo` INT NOT NULL," + 
				"  PRIMARY KEY (`nomeSquadra`, `nomeTorneo`, `idGestoreTorneo`)," + 
//				"  INDEX `fk_Partecipazione_DBTorneo1_idx` (`nomeTorneo` ASC, `idGestoreTorneo` ASC)," + 
				"  CONSTRAINT `fk_Partecipazione_DBSquadra1`" + 
				"    FOREIGN KEY (`nomeSquadra`)" + 
				"    REFERENCES `Calcetto`.`DBSquadra` (`nomeSquadra`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION," + 
				"  CONSTRAINT `fk_Partecipazione_DBTorneo1`" + 
				"    FOREIGN KEY (`nomeTorneo` , `idGestoreTorneo`)" + 
				"    REFERENCES `Calcetto`.`DBTorneo` (`nomeTorneo` , `idGestoreTorneo`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION)" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();

	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
	}
	public static void insert(String nomeS, String nomeT, int idG) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String InsertQuery = "INSERT INTO DBPartecipazione" + "(nomeSquadra,nomeTorneo,idGestoreTorneo) values" + "(?,?,?)";
        try {
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setString(1, nomeS);
            insertPreparedStatement.setString(2, nomeT);
            insertPreparedStatement.setInt(3, idG);

            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBPARTECIPAZIONE";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBPARTECIPAZIONE:");
            while (rs.next()) {
            	 System.out.println("\tnomeSquadra: "+rs.getString("nomeSquadra")+"\tNomeTorneo: "+rs.getString("nomeTorneo")+"\tidGestore: "+rs.getString("idGestoreTorneo"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
		
	}

}