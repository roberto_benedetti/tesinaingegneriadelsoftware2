package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBGirone implements Tabella{
	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBGirone` (" + 
				"  `id` INT NOT NULL," + 
				"  `nomeTorneo` VARCHAR(255) NOT NULL," + 
				"  `idGestoreTorneo` INT NOT NULL," + 
				"  `nomeGirone` VARCHAR(255) NOT NULL," + 
				"  `idGironePadre` INT NULL," + 
				"  PRIMARY KEY (`id`)," + 
				"  UNIQUE INDEX `id_UNIQUE` (`id` ASC)," + 
//				"  INDEX `fk_DBGirone_DBGirone1_idx` (`idGironePadre` ASC)," + 
				"  CONSTRAINT `fk_DBGirone_DBTorneo1`" + 
				"    FOREIGN KEY (`nomeTorneo` , `idGestoreTorneo`)" + 
				"    REFERENCES `Calcetto`.`DBTorneo` (`nomeTorneo` , `idGestoreTorneo`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION," + 
				"  CONSTRAINT `fk_DBGirone_DBGirone1`" + 
				"    FOREIGN KEY (`idGironePadre`)" + 
				"    REFERENCES `Calcetto`.`DBGirone` (`id`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION)" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();
	            
	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
		
	}
	public static void insert(int id,String nomeT,int idGestore,String nomeG,int idGironePadre) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String InsertQuery = "INSERT INTO DBGirone" + "(id,nomeTorneo,idGestoreTorneo,nomeGirone,idGironePadre) values" + "(?,?,?,?,?)";
        try {
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setInt(1, id);
            insertPreparedStatement.setString(2, nomeT);
            insertPreparedStatement.setInt(3, idGestore);
            insertPreparedStatement.setString(4, nomeG);
            insertPreparedStatement.setInt(5, idGironePadre);
            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella GIRONE, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
	public static void insert2(int id,String nomeTorneo,int idGestoreTorneo,String nomeGirone) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String InsertQuery = "INSERT INTO DBGirone" + "(id,nomeTorneo,idGestoreTorneo,nomeGirone) values" + "(?,?,?,?)";
        try {
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setInt(1, id);
            insertPreparedStatement.setString(2, nomeTorneo);
            insertPreparedStatement.setInt(3, idGestoreTorneo);
            insertPreparedStatement.setString(4, nomeGirone);

            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella Girone, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBGIRONE";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBGIRONE:");
            while (rs.next()) {
            	 System.out.println("\tid: "+rs.getInt("id")+"\tnomeTorneo: "+rs.getString("nomeTorneo")+"\tidGestoreTorneo: "+rs.getInt("idGestoreTorneo")+
            			 "\tnomeGirone: "+rs.getString("nomeGirone")+"\tidGironePadre: "+rs.getInt("idGironePadre"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}

}
