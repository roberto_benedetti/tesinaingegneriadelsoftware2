package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBGol implements Tabella{
	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBGol` (" + 
				"  `idGol` INT NOT NULL," + 
				"  `nomeSquadra` VARCHAR(255) NOT NULL," + 
				"  `idGiocatore` INT NOT NULL," + 
				"  `idPartita` INT NOT NULL," + 
				"  `idGirone` INT NOT NULL," + 
				"  PRIMARY KEY (`idGol`)," + 
//				"  INDEX `fk_DBGol_DBSquadra1_idx` (`nomeSquadra` ASC)," + 
//				"  INDEX `fk_DBGol_DBGiocatore1_idx` (`idGiocatore` ASC)," + 
//				"  INDEX `fk_DBGol_DBPartita1_idx` (`idGirone` ASC, `idPartita` ASC)," + 
				"  UNIQUE INDEX `idGol_UNIQUE` (`idGol` ASC)," + 
				"  CONSTRAINT `fk_DBGol_DBSquadra1`" + 
				"    FOREIGN KEY (`nomeSquadra`)" + 
				"    REFERENCES `Calcetto`.`DBSquadra` (`nomeSquadra`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION," + 
				"  CONSTRAINT `fk_DBGol_DBGiocatore1`" + 
				"    FOREIGN KEY (`idGiocatore`)" + 
				"    REFERENCES `Calcetto`.`DBGiocatore` (`idGiocatore`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION," + 
				"  CONSTRAINT `fk_DBGol_DBPartita1`" + 
				"    FOREIGN KEY (`idGirone` , `idPartita`)" + 
				"    REFERENCES `Calcetto`.`DBPartita` (`idGirone` , `idPartita`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION)" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();
	            connessione.commit();

	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
	
	}
	public static void insert(int idGol,String nomeSquadra,int idGiocatore,int idPartita,int idGirone) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
	    PreparedStatement insertPreparedStatement = null;
	    String InsertQuery = "INSERT INTO DBGol" + "(idGol,nomeSquadra,idGiocatore,idPartita,idGirone) values" + "(?,?,?,?,?)";
	    try {
	        connessione.setAutoCommit(false);
	        insertPreparedStatement = connessione.prepareStatement(InsertQuery);
	        insertPreparedStatement.setInt(1, idGol);
	        insertPreparedStatement.setString(2, nomeSquadra);
	        insertPreparedStatement.setInt(3, idGiocatore);
	        insertPreparedStatement.setInt(4, idPartita);
	        insertPreparedStatement.setInt(5, idGirone);
	
	        insertPreparedStatement.executeUpdate();
	        insertPreparedStatement.close();
	        connessione.commit();
	    } catch (SQLException e) {
	        System.out.println("ECCEZIONE Inserimento Tabella Gol, Message " + e.getLocalizedMessage());
	    } catch (Exception e) {
	        e.printStackTrace();
	    } finally {
	        connessione.close();
	    }
	}
	
	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBGOL";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBGOL:");
            while (rs.next()) {
            	 System.out.println("\tidGol: "+rs.getInt("idGol")+"\tnomeSquadra: "+rs.getString("nomeSquadra")+"\tidGiocatore: "+rs.getInt("idGiocatore")+
            			 "\tidPartita: "+rs.getInt("idPartita")+"\tidGirone: "+rs.getInt("idGirone"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}

}

