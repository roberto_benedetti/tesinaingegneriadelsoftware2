package db;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.GregorianCalendar;

public class DBGiocatore implements Tabella {
	@Override
	public void creaTabella() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
		PreparedStatement createPreparedStatement=null;
		String CreateQuery = "CREATE TABLE IF NOT EXISTS `Calcetto`.`DBGiocatore` (" + 
				"  `idGiocatore` INT NOT NULL," + 
				"  `nome` VARCHAR(255) NOT NULL," + 
				"  `cognome` VARCHAR(255) NOT NULL," + 
				"  `dataNascita` DATE NOT NULL," + 
				"  `nomeSquadra` VARCHAR(255) NULL," + 
				"  `cartellino` VARCHAR(255) NOT NULL," + 
				"  PRIMARY KEY (`idGiocatore`)," + 
//				"  INDEX `fk_Giocatore_DBSquadra1_idx` (`nomeSquadra` ASC)," + 
				"  CONSTRAINT `fk_Giocatore_DBSquadra1`" + 
				"    FOREIGN KEY (`nomeSquadra`)" + 
				"    REFERENCES `Calcetto`.`DBSquadra` (`nomeSquadra`)" + 
				"    ON DELETE NO ACTION" + 
				"    ON UPDATE NO ACTION)" + 
				"ENGINE = InnoDB";
		 try {
	            connessione.setAutoCommit(false);
	           
	            createPreparedStatement = connessione.prepareStatement(CreateQuery);
	            createPreparedStatement.executeUpdate();
	            createPreparedStatement.close();
	            
	        } catch (SQLException e) {
	            System.out.println("ECCEZIONE Creazione Tabella, Message " + e.getLocalizedMessage());
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            connessione.close();
	        }
		
	}
	public static void insert(int id,String nome,String cognome, GregorianCalendar data, String nomeS, String cartellino) throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement insertPreparedStatement = null;
        String InsertQuery = "INSERT INTO DBGiocatore" + "(idGiocatore,nome,cognome,dataNascita,nomeSquadra,cartellino) values" + "(?,?,?,?,?,?)";
        try {
        	
            connessione.setAutoCommit(false);
            insertPreparedStatement = connessione.prepareStatement(InsertQuery);
            insertPreparedStatement.setInt(1, id);
            insertPreparedStatement.setString(2, nome);
            insertPreparedStatement.setString(3,cognome);
            insertPreparedStatement.setDate(4, new Date(data.getTimeInMillis()));
            insertPreparedStatement.setString(5, nomeS);	
            insertPreparedStatement.setString(6, cartellino);

            insertPreparedStatement.executeUpdate();
            insertPreparedStatement.close();
            connessione.commit();

        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
	}
	public static void select() throws SQLException {
		Connection connessione = Connessione.getDBConnection();
        PreparedStatement selectPreparedStatement = null;
        String SelectQuery = "select * from DBGIOCATORE";

        try {
            connessione.setAutoCommit(false);
            selectPreparedStatement = connessione.prepareStatement(SelectQuery);
            ResultSet rs = selectPreparedStatement.executeQuery();
            System.out.println("DBGIOCATORE:");
            while (rs.next()) {
            	 System.out.println("\tidGiocatore: "+rs.getInt("idGiocatore")+"\tNome: "+rs.getString("nome")+"\tCognome: "+rs.getString("cognome")+
                 		"\tData: "+rs.getDate("dataNascita")+"\tSquadra: "+rs.getString("nomeSquadra")+"\tCartellino: "+rs.getString("cartellino"));
             }
            selectPreparedStatement.close();


        } catch (SQLException e) {
            System.out.println("ECCEZIONE Inserimento Tabella, Message " + e.getLocalizedMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connessione.close();
        }
		
	}

}