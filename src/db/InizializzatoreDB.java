package db;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedList;

public class InizializzatoreDB {
	private LinkedList<Tabella> lista= new LinkedList<>();
	
	public InizializzatoreDB() throws SQLException{
		DBGestoreTorneo a = new DBGestoreTorneo();
		LinkedList<Tabella> lista = new LinkedList<>();
		lista.add(new DBGestoreTorneo());
		lista.add(new DBTorneo());
		lista.add(new DBSquadra());
		lista.add(new DBGirone());
		lista.add(new DBPartecipazione());
		lista.add(new DBGiocatore());
		lista.add(new DBPartita());
		lista.add(new DBGol());
		this.lista=lista;
		for(Tabella t:lista)
			t.creaTabella();
		
	}
	public static void dropDB() {
	Connection connessione = Connessione.getDBConnection();
    Statement stmt = null;
    try {
        connessione.setAutoCommit(false);
        stmt = connessione.createStatement();
        stmt.execute("DROP TABLE DBTORNEO");		
        stmt.execute("DROP TABLE DBGESTORETORNEO");		
        stmt.execute("DROP TABLE DBGOL");		
        stmt.execute("DROP TABLE DBPARTECIPAZIONE");		
        stmt.execute("DROP TABLE DBPARTITA");
        stmt.execute("DROP TABLE DBSQUADRA");		
        stmt.execute("DROP TABLE DBGIRONE");
        stmt.execute("DROP TABLE DBGIOCATORE");
        stmt.execute("DROP TABLE DBTORNEO");	
        stmt.execute("DROP SCHEMA CALCETTO");		
        stmt.close();
        connessione.commit();

    }catch (Exception e) {
    	// TODO: handle exception
    }
		
	}

}
